#
# Copyright 2020
#
# Author lmy <leemeiyung@sina.com>

# board-specific build parameters
BOOT_MODE	:= qspi
BOARD		:= aiotevm

 # get SoC common build parameters
include plat/nxp/soc-ls1046/soc.mk

BL2_SOURCES	+=	${BOARD_PATH}/ddr_init.c

